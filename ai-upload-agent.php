#!/usr/bin/php
<?php

define("DEBUG_MODE_NO_UPLOAD", false);
define("DEFAULT_TIME_ZONE", "America/New_York" );

$stderr = fopen("php://stderr", "w");
$this_program_name = pathinfo( __FILE__, PATHINFO_BASENAME);
date_default_timezone_set(DEFAULT_TIME_ZONE);

//  Get Folder Path
$folder_level_1_counter = 0;
$folder_level_2_counter = 0;
function getTestUploadPath(){
	global $folder_level_1_counter;
	global $folder_level_2_counter;
    global $sessionId;
    global $thisAgentId;
	if($folder_level_2_counter >= 100){
		$folder_level_1_counter++;
		$folder_level_2_counter=0;
	}
	$folderPath = $sessionId ."/" . $thisAgentId . "/bottom-level-" . $folder_level_1_counter . "/top-level-" . $folder_level_2_counter;
	$folder_level_2_counter++;
	return $folderPath;
}

$test_file_counter = 0;
function getTestFileName(){
    global $test_files;
    global $test_file_counter;
    if($test_file_counter == count($test_files)){
        $test_file_counter = 0;
    }
    $testFileName =  $test_files[$test_file_counter];
    $test_file_counter++;
    return trim($testFileName);
}

//  Do cUrl POST
function doCurlPost($url, $requestProperties){
    global $userId;
    global $password;
    $curlSession = curl_init();
    if (! $curlSession) {
        fputs($stderr, "curdle.php:parseFileFromParam: curl_init() failed\n");
        break;
    }
    curl_setopt($curlSession, CURLOPT_URL, $url);
    curl_setopt($curlSession, CURLOPT_POST, 1);
    curl_setopt($curlSession, CURLOPT_HEADER, 0);
    curl_setopt($curlSession, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curlSession, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($curlSession, CURLOPT_USERPWD, trim($userId) . ":" . trim($password));
    //echo (print_r($requestProperties). "\n");
    curl_setopt($curlSession, CURLOPT_POSTFIELDS, $requestProperties);
    $retVal = "";
    if(!DEBUG_MODE_NO_UPLOAD){
        $retVal = curl_exec($curlSession);
    }
    curl_close($curlSession);
    return $retVal;
}

function writeToConsole($message){
    $t=time();
    echo(date("h:i:s",$t)) . " - " . $message;
}

//----------------- Main logic begins here ------------------

//  Validate the parameters passed on the command line
//if (count($argv) != 7){
  //  fputs($stderr, $this_program_name . ": Incorrect command line arguments. Six are expected.\n");
//    exit(-1);
//} 
//echo "arg0 = " . $argv[0] . "\n";
//echo "arv1 = " . $argv[1] . "\n";
//echo "arg2 = " . $argv[2] . "\n";
//echo "arg3 = " . $argv[3] . "\n";
//echo "arg4 = " . $argv[4] . "\n";
//echo "arg5 = " . $argv[5] . "\n";

//  Read parameters passed from the command line
$configurationFileName = $argv[1];
$sessionId = $argv[2];
$numberOfAgents = $argv[3];
$aggregateUploadRate = $argv[4];
$thisAgentIndex = $argv[5];
	
//  Read parameters from the configuration file
$server_url;
$userId;
$password;
$test_files_path;

writeToConsole("Processing configuration file: ". $configurationFileName."\n\n");
$configFile = fopen($configurationFileName, "r");
if (! $configFile) {
	fputs($stderr, "ERROR: Failed to open configuration file");
    exit(-2);
} 	
$utf8bom = "\xef\xbb\xbf";
while ($line = fgets($configFile)) {
			// Get rid of the possible BOM mark in UTF-8 encoded files - PHP don't like da base!		
	if (substr($line, 0, 3) == $utf8bom) {
		$line = substr($line, 3);
	}
	parse_str($line);
}

//  Calculate initiate upload interval
$initiate_upload_interval = 60 / $aggregateUploadRate * $numberOfAgents;

//	Calculate initial upload delay 
$first_upload_delay = $initiate_upload_interval / $numberOfAgents * $thisAgentIndex;

//  Adjust initiate upload interval so simultansous upload by different agents will sometimes occur.
// TO DO

//	Calculate agent identifier
$thisAgentId = "agent-" . $thisAgentIndex;

//  Read test files from directory
$test_files = scandir ( $test_files_path );
//  - remove . and .. entries from array
array_splice($test_files,0,2);

//  Report all configuration parameters to user
echo("Configuration: ". "\n");
echo "- server_url: " . $server_url. "\n";
echo "- sessionId: " . $sessionId. "\n";
echo "- numberOfAgents: " . $numberOfAgents. "\n";
echo "- thisAgentIndex: " . $thisAgentIndex. "\n";
echo "- aggregateUploadRate: " . $aggregateUploadRate. " per minute\n";
echo "- initiate_upload_interval: " . $initiate_upload_interval. " seconds\n";
echo "- first_upload_delay (for this client): " . $first_upload_delay. " seconds\n";
echo "- test_files_path: " . $test_files_path. "\n";
echo "- test_files: " . "\n";
echo (print_r($test_files). "\n");

//  Do the initial delay
writeToConsole ("Waiting for initial delay of " . trim($first_upload_delay) . " seconds.\n\n");
sleep(trim($first_upload_delay));

// Begin the infinite upload loop
$upload_counter = 0;
$fileName;
$nextUploadInitiationTime = time() + $initiate_upload_interval;
//chdir(trim($test_files_path));

//  Loop forever
do{
	try {
        //  Get name of file to use on this loop
        $fileName = getTestFileName();

        //  Calculate the url for folder to create
        $fileNameNoExtension = substr( $fileName, 0, strrpos ($fileName, "."));
        $fileFolderPath = getTestUploadPath() . "/" . $fileNameNoExtension;
        $fileFolderUrl = trim($server_url) . "/" . $fileFolderPath;

		writeToConsole( "Creating folder at ". ".../" . $fileFolderPath . "\n");
        $requestParameters = [];
        $requestParameters["jcr:primaryType"] = "sling:OrderedFolder";
        $requestParameters["jcr:content/jcr:primaryType"] = "nt:unstructured";
        $retval = doCurlPost( $fileFolderUrl, $requestParameters);
        //echo $retval;

        writeToConsole( "Uploading ". $fileName . "\n");
        $fileUrl = $fileFolderUrl . ".createasset.html";
        $cfile = new CURLFile( "./" . $test_files_path . "/" . $fileName, 'application/postscript', $fileName);
        $curlUploadParameters = [];
        $curlUploadParameters["file"] = $cfile;
        $retval = doCurlPost( $fileUrl, $curlUploadParameters);

        $timeUntilNextUploadTime = $nextUploadInitiationTime - time();
        if($timeUntilNextUploadTime <=0){
            writeToConsole("ERROR: the time to initate the next upload has passed.  Continuing anyway.  Upload wihtout delay\n\n");
        } else {
            writeToConsole( "Waiting " . $timeUntilNextUploadTime . " seconds to initiate next upload.\n\n");
            sleep($timeUntilNextUploadTime);
        }
        $nextUploadInitiationTime = time() + $initiate_upload_interval;
    }
	
	catch (Exception $e) {
		fputs($stderr, $this_program_name . ": throws " . $e . " on line #" . $lineNumber);
	}
	  
}while (true);

?>