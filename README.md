# DIO Load Test Tool

### Usage:

	start-ai-upload-agents <config file> <session label> <number of agents> <aggregate upload rate per minute>
	
### Example:

	start-ai-upload-agents ai-upload-config-LOCALHOST.cfg 2015-07-23-test2 10 5
	
### Result:

- This many separate upload agent processes will be started: <number of agents>
- The combined rate of upload for all agents will be: <aggregate upload rate per minute>
- Each agent will stagger the time at which it initiates upload in order to avoid having all agents upload simultaneously.  
- In AEM, all uploaded files will be located beneath a node named <session label>.  This facilitates analysis of the uploaded content.

The configuration file specified in <config file> has this structure:

	server_url=http://localhost:4502/content/dam/Product/Silhouettes/load_test
	userId=admin
	password=admin
	test_files_path=instrumented-dio-development-files

- Uploaded files are taken from this location on the local machine: <test_files_path> 
- Files are uploaded to <server_url>/<session label>/bottom-level-X/top-level-Y/<uploaded file name>/<uploaded file>.
For example:

![Package Button](https://bytebucket.org/adobeunderarmour/dio-load-test-tool/raw/6e568fd29ce5b827f1fae91488fe2d889eb919a7/images/load-test-result-in-jcr.png)

### Warning: 

Running this tool can cause storage reserved by AEM to increase dramatically.  Therefore you may want to create a backup of AEM before running a load test, and restoring from backup after the test is complete.

### Runtime environment: 

- This is designed to be a cross-platform tool, but it currently works only on windows. 
- To make the run on a *nix platform we need only to create bash equivalents for the very simple programs start-ai-upload-agents.bat and ai-upload-agent.bat.  Anyone with a Mac or Linux machine please feel free to do this.
- The only complicated part of this tool is ai-upload-agent.php.  This can run anywhere php can run.  php runs natively on *nix.  On Windows Cygwin is the recommended runtime environment for php.
- Setup on Windows: 1) Download and install Cygwin, 2) Install optional modules php and php-curl (both from the PHP cateogry shown in Cygwin setup).

