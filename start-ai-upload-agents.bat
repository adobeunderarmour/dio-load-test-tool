@echo off
:: Usage: start-ai-upload-agents <config file> <session label> <number of agents> <aggregate upload rate per minute> 

:: Validate inputs and output usage if not expected number of inputs
set _argcActual=0
set _argcExpected=4
for %%i in (%*) do set /A _argcActual+=1
if %_argcActual% NEQ %_argcExpected% (
	echo Incorrect number of inputs
	echo Usage: start-ai-upload-agents [config file] [session label] [number of agents] [aggregate upload rate per minute]
	echo  e.g.: start-ai-upload-agents ai-upload-config-LOCALHOST.cfg my-session 10 5
  goto:end
)


:: Input parameters
set ai_upload_config_file=%1
set session_label=%2
set num_agents=%3
set upload_rate=%4

:: Batch file configuration parameters

set ai_upload_agent_batch_file=ai-upload-agent.bat
set ai_upload_agent_php_file=ai-upload-agent.php
set cygwinDirPath=\cygwin64\bin

::	The following for debugging
::echo ai_upload_config_file = %ai_upload_config_file% 
::echo session_label = %session_label% 
::echo num_agents = %num_agents% 
::echo upload_rate = %upload_rate% 
::echo ai_upload_agent_batch_file = %ai_upload_agent_batch_file% 
::echo ai_upload_agent_php_file = %ai_upload_agent_php_file%
::echo cygwinDirPath = %cygwinDirPath%

:: Do the work
SET /a i=0

:loop
IF %i%==%num_agents% GOTO END
echo Starting client %i%.
start  %ai_upload_agent_batch_file% %ai_upload_config_file% %session_label% %num_agents% %upload_rate% %i%
SET /a i=%i%+1
GOTO LOOP

:end
